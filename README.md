#The project
This project aims at adding a few buttons in the interface of foundryVTT to allow (badly prepared) GM (like myself) to rescale their maps once they've realized the proportions were off and they've already spent hours working on the various elements of the map.

## Author
This module is brought to you by [jello195](https://gitlab.com/jello195/)

## Installation
For the most up to date version of this module, please use the manifest link below.\
https://gitlab.com/jello195/walls-resize-buttons/-/raw/master/module.json

## Contributions
Any feedback is deeply appreciated. Please issue [tickets](https://gitlab.com/jello195/walls-resize-buttons/-/boards).

## Supported languages
Currently supported languages are :
- English
- French
- Italian
- Spanish
- German
- Portuguese
- Russian
However please post you tickets in english (or french).