console.log('walls-resize-buttons | Before hooks');

// Register the module
Hooks.once('ready', function() {
  console.log('walls-resize-buttons | Initializing my walls button module');

  Hooks.on('getSceneControlButtons', controls => {
    const wallCtrls = [];

    wallCtrls.push({
      name: "resizeWalls",
      title: game.i18n.localize("walls-resize-buttons.menuBtn"),
      icon: "fas fa-arrows-alt",
      button: true,
      onClick: () => {
        let currentValue = 1;

        new Dialog({
          title: game.i18n.localize("walls-resize-buttons.menuTitle"),
          content: `
            <div style="display: flex; flex-direction: column;">
              <div><label for="walls-resize-buttons-sliderValue">${game.i18n.localize("walls-resize-buttons.chooseValueLabel")}</label>
              <output id="walls-resize-buttons-sliderOutput">${currentValue}</output></div>
              <input type="range" id="walls-resize-buttons-sliderValue" name="walls-resize-buttons-sliderValue" min="0.25" max="4" step="0.25" value="${currentValue}">
            </div>
          `,
          buttons: {
            ok: {
              label: game.i18n.localize("walls-resize-buttons.okButtonLabel"),
              callback: (html) => {
                let multiplicateur = parseFloat(html.find('[name="walls-resize-buttons-sliderValue"]').val());
				ui.notifications.info(game.i18n.localize("walls-resize-buttons.loadingMessage"));
				
				let walls = canvas.walls.placeables;
				let updates = [];
				walls.forEach(wall => {
					let newC = [];
					if (wall.document.c && wall.document.c.length === 4) { // Vérifier si les coordonnées sont présentes et valides
						let coords = wall.document.c;
						for (let i = 0; i < 4; i++) {
							// Convertir les coordonnées en nombres entiers
							let newCoord = Math.round(coords[i] * multiplicateur );
							newC.push(newCoord);
						}
						updates.push({
							_id: wall.id,
							c: newC
						});
					} else {
						console.log("Les coordonnées du mur ne sont pas valides :", wall.document.c);
					}
				});
				
				// Modifier les tuiles
				let tiles = canvas.tiles.placeables;
				let tileUpdates = [];
				tiles.forEach(tile => {
					let newSize = {
						width: Math.round(tile.document.width * multiplicateur),
						height: Math.round(tile.document.height * multiplicateur),
						x : Math.round(tile.document.x * multiplicateur),
						y : Math.round(tile.document.y * multiplicateur)
					};
					tileUpdates.push({
						_id: tile.id,
						width: newSize.width,
						height: newSize.height,
						x: newSize.x,
						y: newSize.y
					});
				});


				// Modifier les entrées de journal (ou notes)
				let notes = canvas.notes.placeables;
				let noteUpdates = notes.map(note => {
					let newSize = {
						width: Math.round(note.document.width * multiplicateur),
						height: Math.round(note.document.height * multiplicateur),
						size: Math.round(note.document.size * multiplicateur),
						x: Math.round(note.document.x * multiplicateur),
						y: Math.round(note.document.y * multiplicateur)
					};
					return {
						_id: note.id,
						width: newSize.width,
						height: newSize.height,
						size: newSize.size,
						x: newSize.x,
						y: newSize.y
					};
				});

				// Envoyer les mises à jour différents éléments
				canvas.scene.updatesEmbeddedDocuments("Tile", tileUpdates);
				canvas.scene.updateEmbeddedDocuments("Wall", updates);
				canvas.scene.updateEmbeddedDocuments("Note", noteUpdates);

              }
            }
          },
          render: (html) => {
            html.find('[name="walls-resize-buttons-sliderValue"]').on('input', () => {
              html.find('#walls-resize-buttons-sliderOutput').text(html.find('[name="walls-resize-buttons-sliderValue"]').val());
            });
          }
        }).render(true); // Render the dialog asynchronously
      }
    });

    let wallTools = controls.find(control => control.name === "walls").tools;
    wallTools.push(...wallCtrls);
  });
});